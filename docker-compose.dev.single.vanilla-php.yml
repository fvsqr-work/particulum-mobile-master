version: "3"

services:

#-------------------------------------------------------------------------------
# Nginx is the webserver for the backend
#-------------------------------------------------------------------------------
  nginx:
    build:
      context: "${PROJECTS_PATH}/${BACKEND_APP_DIR}"
      dockerfile: Dockerfile.nginx
    image: particulummobile-dev/backend-nginx
    volumes:
      - "${PROJECTS_PATH}/${BACKEND_APP_DIR}/etc/nginx.site.conf:/etc/nginx/conf.d/default.conf"
      - "${PROJECTS_PATH}/${BACKEND_APP_DIR}:/app"
    ports:
      - 8082:80
    networks:
      backend-net:

#-------------------------------------------------------------------------------
# php-fpm for calling the Zend Expressive app
#-------------------------------------------------------------------------------
  php:
    build:
      context: "${PROJECTS_PATH}/${BACKEND_APP_DIR}"
      dockerfile: Dockerfile.dev.vanilla-php
    image: particulummobile-dev/backend
    labels:
      com.roguewave.particulummobile.description: "Zend Expressive Backend for Particulum Mobile app"
      com.roguewave.particulummobile.service: "particulum-mobile-backend"
    volumes:
      - "${PROJECTS_PATH}/${BACKEND_APP_DIR}:/app"
    networks:
      backend-net:
        aliases:
          - backend-entry-point

#-------------------------------------------------------------------------------
# Redis is used by the Zend Expressive app as a key value storage,
# not for session handling
#-------------------------------------------------------------------------------
  redis:
    image: redis:3.2.7-alpine
    networks:
      backend-net:

#-------------------------------------------------------------------------------
# Nginx container serving the static content;
# Environment variables are written to config.js file at boottime to allowed
# access to backend and for Docker information served by socat through tcp.
#-------------------------------------------------------------------------------
  js:
    build:
      context: ${FRONTEND_REPO}
    image: particulummobile-dev/frontend
    volumes:
      - "${PROJECTS_PATH}/${FRONTEND_APP_DIR}/app:/app"
    environment:
      - BACKEND=http://localhost:8082
      - SWARM_MODE=false
      - CONTAINER_INFO=http://localhost:3000/http://socat:2375/containers/json
    ports:
      - 8081:80
    networks:
      frontend-net:

#-------------------------------------------------------------------------------
# Socat transforms socket connection to tcp allowing to access via http
#-------------------------------------------------------------------------------
  socat:
    image: bobrik/socat
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    ports:
      - 2376:2375
    command: "TCP4-LISTEN:2375,fork,reuseaddr UNIX-CONNECT:/var/run/docker.sock"
    networks:
      frontend-net:

#-------------------------------------------------------------------------------
# CORS container sets the appropriate header as a proxy, so that JavaScript
# can access "socat server" aka Docker daemon
#-------------------------------------------------------------------------------
  cors:
    image: imjacobclark/cors-container
    ports:
      - 3000:3000
    networks:
      frontend-net:

networks:
  backend-net:
  frontend-net:
