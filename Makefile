#!make
include .env
export

PROJECT                      ?= $(shell echo $(PROJECT) | tr -dc '[:alnum:]\n\r' | tr '[:upper:]' '[:lower:]')

DOCKER_COMPOSE_DEV_FILE      ?= docker-compose.dev.yml
DOCKER_COMPOSE_SETUP_FILE    ?= docker-compose.setup.yml

DOCKER                       ?= docker
DOCKER_COMPOSE               ?= docker-compose -p $(PROJECT) -f $(DOCKER_COMPOSE_DEV_FILE)
DOCKER_COMPOSE_SETUP         ?= docker-compose -p $(PROJECT)_setup -f $(DOCKER_COMPOSE_SETUP_FILE)

default: help

#-------------------------------------------------------------------------------
# Build and Release for Production
#-------------------------------------------------------------------------------
build_frontend: ##@Build-and-Release Build Frontend image for production use
	cd $(PROJECTS_PATH)/$(FRONTEND_APP_DIR) && $(MAKE) build

release_frontend: ##@Build-and-Release Release Frontend image to Docker Hub
	cd $(PROJECTS_PATH)/$(FRONTEND_APP_DIR) && $(MAKE) release

build_backend: ##@Build-and-Release Build Backend image for production use
	cd $(PROJECTS_PATH)/$(BACKEND_APP_DIR) && $(MAKE) build

release_backend: ##@Build-and-Release Release Backend image to Docker Hub
	cd $(PROJECTS_PATH)/$(BACKEND_APP_DIR) && $(MAKE) release

build_visualizer: ##@Build-and-Release Build Visualizer image for production use
	cd $(PROJECTS_PATH)/$(VISUALIZER_APP_DIR) && $(MAKE) build

release_visualizer: ##@Build-and-Release Release Visualizer image to Docker Hub
	cd $(PROJECTS_PATH)/$(VISUALIZER_APP_DIR) && $(MAKE) release

build_all: build_frontend build_backend build_visualizer ##@Build-and-Release Build all images for production use
release_all: release_frontend release_backend release_visualizer ##@Build-and-Release Release all images to Docker Hub

#-------------------------------------------------------------------------------
# Helper
#-------------------------------------------------------------------------------
docker-info:
	@printf "\n%-60s %-20s\n" "Name" "ContainerID"
	@echo "--------------------------------------------------------------------------------"
	@$(DOCKER) inspect --format "{{ .Name }} {{.Config.Hostname }}" $(shell $(DOCKER_COMPOSE) ps -q) | awk '{ printf "%-60s %-20s\n", $$1, $$2}'
	@echo

docker-up:
	$(DOCKER_COMPOSE) up -d
	$(DOCKER_COMPOSE) ps

docker-logs:
	$(DOCKER_COMPOSE) logs -f

info: docker-info ##@docker show running container information

logs: docker-logs ##@docker display combined logs of all running containers

#-------------------------------------------------------------------------------
# Swarm Only
#-------------------------------------------------------------------------------
update-stack-pm-vanilla: deploy-stack-pm-vanilla ##@swarm update vanilla php stack with content from compose yml file
deploy-stack-pm-vanilla: ##@swarm deploy vanilla php stack with content from compose yml file
	docker stack deploy --compose-file docker-compose.prod.swarm.vanilla-php.yml pm-vanilla
halt-stack-pm-vanilla: ##@swarm scale all services from vanilla php stack to 0
	docker service scale pm-vanilla_nginx=0 pm-vanilla_php=0 pm-vanilla_js=0 pm-vanilla_redis=0 pm-vanilla_vis=0
up-stack-pm-vanilla: ##@swarm scale all services from vanilla php stack to 1
	docker service scale pm-vanilla_nginx=1 pm-vanilla_php=1 pm-vanilla_js=1 pm-vanilla_redis=1 pm-vanilla_vis=1
scale-stack-pm-vanilla-to-5: ##@swarm scale php service from vanilla php stack stack to 5
	docker service scale pm-vanilla_php=5

update-stack-pm-zs: deploy-stack-pm-zs ##@swarm update Zend Server stack with content from compose yml file
deploy-stack-pm-zs: ##@swarm deploy Zend Server stack with content from compose yml file
	docker stack deploy --compose-file docker-compose.prod.swarm.zend-server.yml pm-zs
halt-stack-pm-zs: ##@swarm scale all services from Zend Server stack to 0
	docker service scale pm-zs_php=0 pm-zs_js=0 pm-zs_DB=0 pm-zs_redis=0 pm-zs_lb-ui=0 pm-zs_vis=0
up-stack-pm-zs: ##@swarm scale all services from Zend Server stack to 1
	docker service scale pm-zs_php=1 pm-zs_js=1 pm-zs_DB=1 pm-zs_redis=1 pm-zs_lb-ui=1 pm-zs_vis=1
scale-stack-pm-zs-to-5: ##@swarm scale php service from Zend Server stack stack to 5
	docker service scale pm-zs_php=5

update-stack-pm-micro: deploy-stack-pm-micro ##@swarm update vanilla micro php stack with content from compose yml file
deploy-stack-pm-micro: ##@swarm deploy vanilla micro php stack with content from compose yml file
	docker stack deploy --compose-file docker-compose.prod.swarm.vanilla-php.micro.yml pm-micro
halt-stack-pm-micro: ##@swarm scale all services from vanilla micro php stack to 0
	docker service scale pm-micro_nginx=0 pm-micro_php=0 pm-micro_php-color=0 pm-micro_js=0 pm-micro_redis=0 pm-micro_vis=0
up-stack-pm-micro: ##@swarm scale all services from vanilla micro php stack to 1
	docker service scale pm-micro_nginx=1 pm-micro_php=1 pm-micro_php-color=1 pm-micro_js=1 pm-micro_redis=1 pm-micro_vis=1
scale-stack-pm-micro-to-5: ##@swarm scale php service from vanilla micro php stack to 5
	docker service scale pm-micro_php=5

#-------------------------------------------------------------------------------
# Development
#-------------------------------------------------------------------------------
scale-to-1: ##@scaling scale to 1 PHP containers
	$(DOCKER_COMPOSE) up -d --scale php=1

scale-to-3: ##@scaling scale to 3 PHP containers
	$(DOCKER_COMPOSE) up -d --scale php=3

scale-to-5: ##@scaling scale to 5 PHP containers
	$(DOCKER_COMPOSE) up -d --scale php=5

scale-to-10: ##@scaling scale to 10 PHP containers
	$(DOCKER_COMPOSE) up -d --scale php=10
#-------------------------------------------------------------------------------
# Development
#-------------------------------------------------------------------------------
setup: ##@dev prepares application. Has to be called initially, can be called for updating
	$(DOCKER_COMPOSE_SETUP) run --rm create-projects-dir
	$(DOCKER_COMPOSE_SETUP) run --rm backend-checkout
	$(DOCKER_COMPOSE_SETUP) run --rm backend-composer
	$(DOCKER_COMPOSE_SETUP) run --rm frontend-checkout
	$(DOCKER_COMPOSE_SETUP) run --rm visualizer-checkout

build-dev:	##@dev build docker images for dev
	$(DOCKER_COMPOSE) build

up: docker-up docker-info	##@dev start containers with docker-compose and display some status info

down:	##@dev stop all running containers, started with docker-compose
	$(DOCKER_COMPOSE) stop

start: setup build-dev up ##@dev prepare app, build containers and start them

clean:  ##@dev remove application containers
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) rm -fv
	-$(DOCKER_COMPOSE) down

#-------------------------------------------------------------------------------
# Help
#
# Help based on https://gist.github.com/prwhite/8168133 thanks to @nowox and @prwhite
# And add help text after each target name starting with '\#\#'
# A category can be added with @category
#-------------------------------------------------------------------------------

HELP_FUN = \
		%help; \
		while(<>) { push @{$$help{$$2 // 'options'}}, [$$1, $$3] if /^([\w-]+)\s*:.*\#\#(?:@([\w-]+))?\s(.*)$$/ }; \
		print "\nusage: make [target ...]\n\n"; \
	for (keys %help) { \
		print "$$_:\n"; \
		for (@{$$help{$$_}}) { \
			$$sep = "." x (35 - length $$_->[0]); \
			print "  $$_->[0] $$sep$$_->[1]\n"; \
		} \
		print "\n"; }

help:				##@system show this help
	#
	# General targets
	#
	@perl -e '$(HELP_FUN)' $(MAKEFILE_LIST)
