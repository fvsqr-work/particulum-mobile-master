var containers = {};

function displayContainerInLegend(containerId, state) {
  containers[containerId] = {
      "state": state
  };

  if ($("#cont_" + containerId).length == 0) {
      if (state != "running") {
          $("#cont_" + containerId).remove();
          return;
      }
      $("#container_legend").append("<div id='cont_" + containerId + "' class='hostid legend'></div>");
      $("#cont_" + containerId).append("<div class='container-id'>" + containerId + "</div>");
      $("#cont_" + containerId).append("<div class='container-colission-count'>0</div>");
      $.ajax({
          method: 'POST',
          url: BACKEND + "/particle-color-by-container-id",
          data: {
              container_id: containerId
          },
          xhrFields: {
              withCredentials: true
          },
          dataType: 'json',
          success: function(data, textStatus, request) {
              $("#cont_" + containerId).css('border-color', "#" + ('000000' + data.color).substring(data.color.length));
          }
      });
    }
}

function removeObsoleteContainersFromLegend() {
    $("#container_legend div div.container-id").each(function(index) {
        contId = $(this).text();
        if (typeof containers[contId] != 'undefined' && containers[contId].state == 'running')
            return;

        $("#cont_" + contId).remove()
    });
}

function containerInfoDockerSwarm(serviceId) {
    var svcId = serviceId;
    $.ajax({
        url: CONTAINER_INFO + "/apis/tasks",
        success: function(data, textStatus, request) {
            var items = data.objects;
            containers = {};

            items.forEach(function(item) {
                if (typeof item.ServiceID == 'undefined' || item.ServiceID != svcId) {
                    return false;
                }

                var state = item.Status.State;
                if (state == 'pending') return;
                
                var containerId = item.Status.ContainerStatus.ContainerID.substr(0, 12);

                displayContainerInLegend(containerId, state);
            });

            removeObsoleteContainersFromLegend();
        }
    });
    setTimeout(function() {
        containerInfoDockerSwarm(svcId);
    }, 1000);
}

function containerInfoDocker() {
    $.ajax({
        url: CONTAINER_INFO,
        success: function(items, textStatus, request) {
            if (!$.isArray(items)) return;

            containers = {};

            $('#container_legend').removeClass('hideme');

            items.forEach(function(item) {
                if (item.Labels["com.roguewave.particulummobile.service"] != "particulum-mobile-backend") return false;

                var containerId = item.Id.substr(0, 12);
                var state = item.State;

                displayContainerInLegend(containerId, state);
            });

            removeObsoleteContainersFromLegend();
        }
    });

    setTimeout(function() {
        containerInfoDocker();
    }, 1000);
}

function containerInfo() {
    if (SWARM_MODE) {
        $.ajax({
            url: CONTAINER_INFO + "/apis/services",
            success: function(data, textStatus, request) {
                var items = data.objects;

                if (!$.isArray(items)) return;

                $('#container_legend').removeClass('hideme');

                items.forEach(function(item) {
                    if (typeof item.Spec.TaskTemplate.ContainerSpec.Labels == 'undefined'
                      || item.Spec.TaskTemplate.ContainerSpec.Labels["com.roguewave.particulummobile.service"] != "particulum-mobile-backend"
                      || item.Spec.Labels["com.docker.stack.namespace"] != SWARM_STACK_NAMESPACE) {
                        return false;
                    }

                    var backendServiceId = item.ID;
                    containerInfoDockerSwarm(backendServiceId);
                });
            }
        });
    }
    else {
        containerInfoDocker();
    }
};
